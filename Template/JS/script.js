// we select the playerKeyboard and store it in a constant
const playerKeyboard = document.querySelector("#duck");

//set the default position of the duck
let posTop = 10;
let posLeft = 10;

// make lets for the scores
let duckScore = 0;
let dogScore = 0;

// propmt user for their names
const duckPlayer = prompt("What is the Duck players name? ");
const dogPlayer = prompt("What is the Dog players name? ");

// create the const to add the names to the page
document.querySelector(".key").append(duckPlayer);
document.querySelector(".mou").append(dogPlayer);

// Add a const for restart id called end page
const endPage = document.getElementById("restart");

// add a const for both score elements
const scoreMou = document.getElementById("scoreM");
const scoreKey = document.getElementById("scoreK");

let fast = 3;

// Make the function for moving
function move(event) {
  // when s is down, and the top position is less than 90
  if (event.key == "s" && posTop < 95) {
    // move down
    posTop += fast;
    // when w is down, and the top position is more than 0
  } else if (event.key == "w" && posTop > 0) {
    // move up
    posTop -= fast;
    // when s is down, and the left position is less than 0
  } else if (event.key == "a" && posLeft > 0) {
    // move left
    posLeft -= fast;
    // invert image
    playerKeyboard.setAttribute(
      "src",
      "./img/85003b8e414d2708f18fcb0fd1ccecf0 (copy).png"
    );
    // when d is down, and the left position is less than 90
  } else if (event.key == "d" && posLeft < 95) {
    // move right
    posLeft += fast;
    // invert image
    playerKeyboard.setAttribute(
      "src",
      "./img/85003b8e414d2708f18fcb0fd1ccecf0.png"
    );
    // If top position is more than 90
  } else if (posTop >= 95) {
    // move to top
    posTop = 0;
    // If left position is more than 90
  } else if (posLeft >= 95) {
    // move to left
    posLeft = 0;
    // If top position is less than 0
  } else if (posTop <= 0) {
    // move to bottom
    posTop = 95;
    // If left position is less than 0
  } else if (posLeft <= 0) {
    // move to right
    posLeft = 95;
  }
  //updates the style value to %
  playerKeyboard.style.top = posTop + "%";
  playerKeyboard.style.left = posLeft + "%";
}

// adds an event listener to key down
document.addEventListener("keydown", move);

// Add buttons for speed
const up = document.getElementById("up");
const down = document.getElementById("down");

// funtions for speed
function speedUp() {
  fast++;
  console.log(fast);
}
function speedDown() {
  fast--;
  console.log(fast);
}

//listener for speed up and down 
up.addEventListener("click", speedUp);
down.addEventListener("click", speedDown);

// new function touched
function touched() {
  //change image to dead duck
  playerKeyboard.setAttribute("src", "./img/logo-duck-hit.png");
  //removed event listener for move
  document.removeEventListener("keydown", move);
  //removed event listener for touched
  playerKeyboard.removeEventListener("click", touched);
  //removed event listener for start
  startButton.removeEventListener("click", startTimer);
  // cleares the clock
  clearInterval(countdownTimer);
  // message to user
  alert("Dog wins one point");
  //shows the restart page
  endPage.style.display = "initial";
  // adds one point to dog
  dogScore++;
  // adds the dog score to the page
  scoreMou.innerText = dogPlayer + ": " + dogScore;
  // adds the duck score to the page
  scoreKey.innerText = duckPlayer + ": " + duckScore;
  //Congrats on win game
  if (dogScore >= 3) {
    alert("Dog wins the game");
  } else {
  }
}
// event listener for touched
playerKeyboard.addEventListener("click", touched);

/* Now to create the timer */

// add const for button and timer
const startButton = document.getElementById("start");
const timer = document.getElementById("timer");

// define the time left
let timeLeft = 30;
let countdownTimer;

// function updates the text content of an HTML element with the id "timer" to display the current `timeLeft` value
function updateTimer() {
  timer.textContent = timeLeft;
}

//function for the countdown
function countdown() {
  // if time left is more than 0
  if (timeLeft > 0) {
    // minus one from time left
    timeLeft--;
    // then update the timer
    updateTimer();
  } else {
    // basically stops the interval in the startTimer function
    clearInterval(countdownTimer);
    // message duck player
    alert("Duck player gets one point");
    // changes the image of the bird
    playerKeyboard.setAttribute("src", "./img/clipart4287261.png");
    //removed event listener for move
    document.removeEventListener("keydown", move);
    //removed event listener for touched
    playerKeyboard.removeEventListener("click", touched);
    //removed event listener for start
    startButton.removeEventListener("click", startTimer);
    // pull up the restart page
    restart.style.display = "initial";
    //Gives point to duck
    duckScore++;
    // adds the score to the page
    scoreMou.innerText = dogPlayer + ": " + dogScore;
    // adds the score to the page
    scoreKey.innerText = duckPlayer + ": " + duckScore;
    //Congrats on win game
    if (duckScore >= 3) {
      alert("Duck wins the game");
    } else {
    }
  }
}

//function for the start click
function startTimer() {
  // set the coundown interval to go down my 1000 mili seconds
  countdownTimer = setInterval(countdown, 1000);
  // remove the event listener for startTimer
  startButton.removeEventListener("click", startTimer);
}

// adds listener to the start button
startButton.addEventListener("click", startTimer);

function reset() {
  // Remove the restart page
  restart.style.display = "none";
  // adds listener to the start button
  startButton.addEventListener("click", startTimer);
  // event listener for touched
  playerKeyboard.addEventListener("click", touched);
  // adds an event listener to key down
  document.addEventListener("keydown", move);
  // changes the image of the bird
  playerKeyboard.setAttribute(
    "src",
    "./img/85003b8e414d2708f18fcb0fd1ccecf0.png"
  );
  // change time left
  timeLeft = 30;
  // update timer
  updateTimer();
  //initialize position
  posTop = 10;
  posLeft = 10;
  //updates the style value to %
  playerKeyboard.style.top = posTop + "%";
  playerKeyboard.style.left = posLeft + "%";
}
// add a const for the restart button
const restartButton = document.getElementById("restart_button");

// add event listener for restart button
restartButton.addEventListener("click", reset);
